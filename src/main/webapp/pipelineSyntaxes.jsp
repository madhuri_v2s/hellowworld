<!DOCTYPE html>
<html>
<title>Jenkis File Demo</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body, h1 {
	font-family: "Raleway", sans-serif;
	color: #000000
}

body, html {
	height: 100%
}

.bgimg {
	background-image: url('JenkinsAsBG.jpg');
	min-height: 100%;
	background-position: inherit;
	background-size: cover;
}

.cdpipeline-img {
	margin: auto;
	width: 72%;
	padding-left: 20%;
}
</style>
<body>

	<div
		class="bgimg w3-display-container w3-animate-opacity w3-text-white">
		<div class="w3-display-middle">
			<ol type="1">
				<li><b>Declarative pipeline syntax :</b></li>
				<img alt="CDPipeline" src="declarativePipelineSyntax.png" class="declarativepipelinesyntax-img"/>

				<li><b>Scripted pipeline syntax :</b></li>
				<img alt="CDPipeline" src="scriptivePipelineSyntax.png" class="declarativepipelinesyntax-img"/>

			</ol>
		</div>
		
		<div class="w3-display-topleft w3-padding-large">By Madhuri</div>
		<div class="w3-display-topright w3-padding-large">
			<a href="jenkinsFileAndExample.jsp"
				style="text-underline-position: under;"><span
				class="jenkinsfile-link" title="Click to go home page"><i
					class="fa fa-chevron-left" style="font-size: 15px; color: #ff6464"></i>
					Jenkins File</span></a>
		</div>

	</div>

</body>
</html>