<!DOCTYPE html>
<html>
<title>Jenkis Pipeline Demo</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body, h1 {
	font-family: "Raleway", sans-serif;
	color: #000000
}

body, html {
	height: 100%
}

.bgimg {
	background-image: url('JenkinsAsBG.jpg');
	min-height: 100%;
	background-position: inherit;
	background-size: cover;
}

.cdpipeline-img {
	margin: auto;
	width: 72%;
	padding-left: 20%;
}
</style>
<body>

	<div
		class="bgimg w3-display-container w3-animate-opacity w3-text-white">
		<div class="w3-display-middle">
			<h1 class="w3-jumbo w3-animate-top">Jenkins Pipeline</h1>
			<hr class="w3-border-grey" style="margin: auto; width: 55%">
			<p class="w3-large">In Jenkins, a pipeline is a group of events
				or jobs which are interlinked with one another in a sequence. In
				simple words, Jenkins Pipeline is a combination of plugins that
				support the integration and implementation of continuous delivery
				pipelines using Jenkins.</p>
			<img alt="CDPipeline" src="CDPipeline.png" class="cdpipeline-img">
			<p class="w3-large">
				The picture above represents a <b>continuous delivery pipeline</b>
				in Jenkins. It contains a group of states called <b>build,
					deploy, test and release</b>. These events are interlinked with each
				other. Every state has its events, which work in a sequence called a
				continuous delivery pipeline. A continuous delivery pipeline is an
				automated expression, it involves developing the software in a
				reliable and repeatable manner, and progression of the built
				software through multiple stages of testing and deployment.Jenkins
				pipelines can be defined using a text file called <a
					href="jenkinsFileAndExample.jsp"><span class="jenkinsfile-link"
					title="Click to know what is jenkinsfile">JenkinsFile</span></a>.
			</p>

		</div>
		<div class="w3-display-topleft w3-padding-large">By Madhuri</div>
	</div>

</body>
</html>
