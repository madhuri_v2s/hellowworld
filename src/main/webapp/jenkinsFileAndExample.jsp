<!DOCTYPE html>
<html>
<title>Jenkis File Demo</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body, h1 {
	font-family: "Raleway", sans-serif;
	color: #000000
}

body, html {
	height: 100%
}

.bgimg {
	background-image: url('JenkinsAsBG.jpg');
	min-height: 100%;
	background-position: inherit;
	background-size: cover;
}

.cdpipeline-img {
	margin: auto;
	width: 72%;
	padding-left: 20%;
}
</style>
<body>

	<div
		class="bgimg w3-display-container w3-animate-opacity w3-text-white">
		<div class="w3-display-middle">
			<h1 class="w3-jumbo w3-animate-top" style="padding-left: 32%;">Jenkins
				File</h1>
			<hr class="w3-border-grey" style="margin: auto; width: 43%">
			<p class="w3-large">In Jenkins, a pipeline is a group of events
				or jobs which are interlinked with one another in a sequence. In
				simple words, Jenkins Pipeline is a combination of plugins that
				support the integration and implementation of continuous delivery
				pipelines using Jenkins.</p>
			<h4 style="font-size: 18px !important;">The benefits of using
				JenkinsFile are:</h4>
			<ul type="disc">
				<li>You can create pipelines automatically for all branches and
					execute pull requests with just one JenkinsFile.</li>
				<li>You can review your code on the pipeline.</li>
				<li>This is the singular source for your pipeline and can be
					modified by multiple users.</li>
			</ul>
			<h4 style="font-size: 18px !important;">The Jenkins pipeline is
				written based on two syntaxes, namely:</h4>
			<ol type="1">
				<li><b>Declarative pipeline syntax :</b> Declarative pipeline
					is a relatively new feature that supports the pipeline as code
					concept. It makes the pipeline code easier to read and write. This
					code is written in a Jenkinsfile which can be checked into a source
					control management system such as Git. The declarative pipeline is
					defined within a block labelled 'pipeline'</li>
				<li><b>Scripted pipeline syntax :</b> The scripted pipeline is
					a traditional way of writing the code. In this pipeline. Scripted
					pipeline uses stricter groovy based syntaxes. Scripted pipeline is
					defined within a 'node'.</li>
					<a href="pipelineSyntaxes.jsp"><span class="jenkinsfile-link"
				title="Click to know pipeline syntaxes">Pipeline Syntaxes</span></a>
			</ol>
			
			<!-- <img alt="CDPipeline" src="CDPipeline.png" class="cdpipeline-img"> -->

		</div>
		<div class="w3-display-topleft w3-padding-large">By Madhuri</div>
		<div class="w3-display-topright w3-padding-large">
			<a href="index.jsp" style="text-underline-position: under;"><span
				class="jenkinsfile-link" title="Click to go home page"><i
					class="fa fa-chevron-left" style="font-size: 15px; color: #ff6464"></i>
					Jenkins Pipeline</span></a>
		</div>

	</div>

</body>
</html>